﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Execom.ErrorHandling.ViewModel
{
    public class ThreadingViewModel : ViewModelBase
    {
        private TaskCompletionSource<bool> _internetStatusTaskCompletionSource;

        public ThreadingViewModel()
        {
            MustRunSynchronouslyOperation();

            // RelayCommand is wrapper around action which should be executed
            // when user clicks a button.
            AsyncCommand = new RelayCommand(OnAsyncCommand);
            SendMessageCommand = new RelayCommand(OnSendMessageCommand);
            InternetCommand = new RelayCommand(OnInternetCommand);

            // Fire and forget.
            // Do not change this call.
            Task.Run(() => Initialize());
        }

        public RelayCommand AsyncCommand { get; set; }

        public RelayCommand SendMessageCommand { get; set; }

        public RelayCommand InternetCommand { get; set; }

        // Do not change methods signature
        private void MustRunSynchronouslyOperation()
        {
            try
            {
                int reslut = TaskThatThrows().Result;
                // we want to catch only ArgumentNullException and NullReferenceException
                // everything else should be ignored...
            }
            catch (ArgumentNullException ex) 
            {
                Console.WriteLine($"MustRunSynchronouslyOperation failed with exception: {ex}");
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine($"MustRunSynchronouslyOperation failed with exception: {ex}");
            }
        }

        // Do not edit this method.
        private Task<int> TaskThatThrows()
        {
            Exception[] exceptions = { new NullReferenceException(), new ArgumentNullException() };
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            tcs.SetException(exceptions);
            return tcs.Task;
        }

        private void Initialize()
        {
            try
            {
                DoFirstThingAsync();
                DoSecondThingAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Initialize failed with exception: {ex}");
            }
        }

        // Do not edit this method.
        private Task DoFirstThingAsync() => Task.Delay(700).ContinueWith(_ => throw new NotImplementedException());

        // Do not edit this.
        private Task DoSecondThingAsync() => throw new NotImplementedException();

        private void OnAsyncCommand()
        {
            try
            {
                SomeAsyncThrowableAction();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"AsyncCommand failed. Reason: {ex}");
            }
        }

        private async void SomeAsyncThrowableAction()
        {
            // Do not change this
            Task SomeHeavyWork()
            {
                Thread.Sleep(1000);
                throw new Exception("I throw up!");
            }

            await SomeHeavyWork();
        }

        private async void OnSendMessageCommand()
        {
            Task task = Task.WhenAll(SendMessageAsync(), DoSecondThingAsync());
            try
            {
                await task;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error while sending! {ex}");
                Console.WriteLine(ex);
            }
        }

        // Do not edit this method.
        private Task SendMessageAsync()
        { 
            string ConstructMessage()
            {
                return "Some message";
            }

            return CheckInternetAndSendMessage(ConstructMessage());
        }

        private Task CheckInternetAndSendMessage(string message)
        {
            // Simulates internet check that takes 500ms to execute and throws exception at the end.
            Task.Delay(500).ContinueWith(_ => throw new ArgumentException());
            Console.WriteLine($"Message sent: {message}");
            return Task.CompletedTask;
        }

        private async void OnInternetCommand()
        {
            var changed = await AwaitInternetChangeStatus();
            MessageBox.Show("IP changed = " + changed);
        }

        // Do not change methods signature
        private async Task<bool> AwaitInternetChangeStatus()
        {
            _internetStatusTaskCompletionSource = new TaskCompletionSource<bool>();
            NetworkChange.NetworkAddressChanged += OnNetworkChanged;

            try
            {
                // Represents some additional operation that needs to be done
                // Please modify only ContinueWith();
                await TaskThatThrows().ContinueWith(task =>
                {
                    // TODO notify consumer that task failed!
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine($"AwaitInternetChangeStatus throw exception before address was changed! Reason: {ex}");
            }
            return await _internetStatusTaskCompletionSource.Task;
        }

        // Do not edit this method.
        private void OnNetworkChanged(object sender, EventArgs eventArgs)
        {
            NetworkChange.NetworkAddressChanged -= OnNetworkChanged;
            _internetStatusTaskCompletionSource.SetResult(true);
            Console.WriteLine("Address changed!");
        }
    }
}
