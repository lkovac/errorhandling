/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:Execom.ErrorHandling"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using CommonServiceLocator;
using Execom.ErrorHandling.Services;
using Execom.ErrorHandling.Services.Interface;
using FunkyErrorHandler;
using FunkyErrorHandler.Builder;
using FunkyErrorHandler.Builder.Interface;
using FunkyErrorHandler.Strategy;
using FunkyErrorHandler.Strategy.Interface;
using GalaSoft.MvvmLight.Ioc;

namespace Execom.ErrorHandling.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<ThreadingViewModel>();
            SimpleIoc.Default.Register<IMessageService, MessageService>();
            SimpleIoc.Default.Register<FunctionalErrorHandler>();
            SimpleIoc.Default.Register<IMessenger, Messenger>();
            SimpleIoc.Default.Register<IBuilderHeader, Builder>();
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public ThreadingViewModel Threading
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ThreadingViewModel>();
            }
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}