﻿using FunkyErrorHandler.Builder.Model;
using FunkyErrorHandler.Strategy;
using System;

namespace Execom.ErrorHandling.Services.Interface
{
    public interface IMessageService
    {
        Message CreateMessage();

        bool SendMessage(Message msg, string to, DeliveryType delivery);

        string SendMessage(string input);

        string BuildMessage(Func<string> funcHandle, Func<Exception, string> exceptionHandleAction);

        Func<Func<int, int, bool>, Action<Exception>, bool> CompareInts { get; }

        Func<Func<string, string>, int, Action<Exception>, string> GetStringAsyncWithRetry { get; }
    }
}
