﻿using System;
using Execom.ErrorHandling.Services.Interface;
using FunkyErrorHandler;
using FunkyErrorHandler.Builder.Model;
using FunkyErrorHandler.Strategy;

namespace Execom.ErrorHandling.Services
{
    public class MessageService : IMessageService
    {
        private readonly FunctionalErrorHandler _handler;

        public MessageService(FunctionalErrorHandler handler) =>
            _handler = handler ?? throw new ArgumentNullException(nameof(handler));

        public Func<Func<int, int, bool>, Action<Exception>, bool> CompareInts => 
            _handler.CompareInts;

        public Func<Func<string, string>, int, Action<Exception>, string> GetStringAsyncWithRetry =>
            _handler.GetStringAsyncWithRetry;

        public string BuildMessage(Func<string> funcHandle, Func<Exception, string> exceptionHandleAction) =>
            _handler.BuildMessage(funcHandle, exceptionHandleAction);

        public Message CreateMessage() =>
            _handler.CreateMessage();

        public string SendMessage(string input) =>
            _handler.SendMessage(input);

        public bool SendMessage(Message msg, string to, DeliveryType delivery) =>
            _handler.SendMessage(msg, to, delivery);
    }
}
