﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace Execom.ErrorHandling.Helpers
{
    public class WebClientWrapper
    {
        private bool _throwException;
        public WebClientWrapper()
        {

        }

        public Task<string> DownloadStringAsync()
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    return DwString();
                }
                catch (Exception)
                {
                    Console.WriteLine("Downloading failed!!!");
                    throw;
                }
            }
        }

        public async Task<string> DwString()
        {
            await Task.Delay(2000);
            #region Exception throwing simulation
            if (_throwException)
            {
                _throwException = !_throwException;
                throw new ArgumentException();
            }
            _throwException = !_throwException;
            #endregion
            return "Downloaded string!";
        }
    }
}
