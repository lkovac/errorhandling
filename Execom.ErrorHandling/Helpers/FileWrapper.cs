﻿using System;
using System.IO;

namespace Execom.ErrorHandling.Helpers
{
    public class FileWrapper
    {
        bool _throwUnauthorized;

        public void WriteAllText(string path, string content)
        {
            try
            {
                #region Exception throwing simulation
                if (_throwUnauthorized)
                {
                    _throwUnauthorized = !_throwUnauthorized;
                    throw new UnauthorizedAccessException($"No access to file!");
                }
                _throwUnauthorized = !_throwUnauthorized;
                #endregion
                File.WriteAllText(path, content);
            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine($"Access not possible! {ex.Message}");
                throw ex;
            }
        }
    }
}
