﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Execom.ErrorHandling.Views
{
    /// <summary>
    /// Interaction logic for PersonView.xaml
    /// </summary>
    public partial class ThreadingView : UserControl
    {
        private const string _testURL = @"https://gitlab.com/users/sign_in";
        private static int _throwErrorHint = 1;

        public ThreadingView()
        {
            InitializeComponent();

            // do not change this into command
            onlineDataButton.Click += (s, e) =>
            {
                string data = string.Empty;
                try
                {
                    data = GetOnlineData().Result;
                }
                catch (InvalidOperationException ex)
                {
                    Console.WriteLine($"Get online data operation failed. Reason: {ex.Message}");
                }
                MessageBox.Show("Received data: " + data);
            };
        }

        private Task<string> GetOnlineData()
        {
            if (_throwErrorHint % 2 == 0)
            {
                throw new InvalidOperationException("Whoops!");
            }

            _throwErrorHint++;

            using (var httpClient = new HttpClient())
            {
                return httpClient.GetStringAsync(_testURL);
            }
        }
    }
}
