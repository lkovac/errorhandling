﻿using Execom.ErrorHandling.Helpers;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Execom.ErrorHandling.Views
{
    /// <summary>
    /// Interaction logic for NikolaPage.xaml
    /// </summary>
    public partial class SaveDownloadPage : UserControl
    {
        private FileWrapper _fileWrapper;
        private WebClientWrapper _webWrapper;

        public SaveDownloadPage()
        {
            InitializeComponent();
            _fileWrapper = new FileWrapper();
            _webWrapper = new WebClientWrapper();
        }

        private async void Download_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine("Downloading content from web...");
                Content.Text = await _webWrapper.DownloadStringAsync();
                Console.WriteLine("Successfully downloaded!");
            }
            catch (Exception)
            {
                MessageBox.Show("Download failed!");
            }
        }

        private void Save_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine($"Saving content to {Path.Text} file...");
                _fileWrapper.WriteAllText(Path.Text, Content.Text);
                Console.WriteLine($"File successfully saved!");
            }
            catch (ArgumentException ex)
            {
                if (ex is ArgumentNullException)
                {
                    Console.WriteLine(ex.Message);
                }
                else
                {
                    MessageBox.Show(ex.Message);
                    Console.WriteLine(ex.Message);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                if (ex.Message == "No access to file!")
                {
                    MessageBox.Show(ex.Message);
                    Console.WriteLine(ex.Message);
                }
                else
                {
                    throw ex;
                }
            }

        }
    }
}
