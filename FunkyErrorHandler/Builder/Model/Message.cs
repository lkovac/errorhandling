﻿namespace FunkyErrorHandler.Builder.Model
{
    public class Message
    {
        public string MessageHeader { get; set; }

        public string MessageBody { get; set; }

        public string MessageFooter { get; set; }

        public override string ToString()
        {
            return $"Header: {MessageHeader} Body: {MessageBody} Footer:{MessageFooter}";
        }
    }
}
