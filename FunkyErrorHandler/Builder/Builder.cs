﻿using FunkyErrorHandler.Builder.Interface;
using FunkyErrorHandler.Builder.Model;

namespace FunkyErrorHandler.Builder
{
    public class Builder : IBuilder, IBuilderHeader, IBuilderBody, IBuilderFooter
    {
        public string Header { get; set; }

        public string Body { get; set; }

        public string Footer { get; set; }


        public IBuilderBody BuildHeader()
        {
            return new Builder
            {
                Header = "Hi There!"
            };
        }

        public IBuilderFooter BuildBody()
        {
            return new Builder
            {
                Header = Header,
                Body = "This is just a sample!"
            };

        }

        public IBuilder BuildFooter()
        {
            try
            {
                throw new System.Exception();
            }
            catch
            {
                return new Builder
                {
                    Header = Header,
                    Body = Body,
                    Footer = string.Empty
                };
            }
        }

        public Message Build()
        {
            return new Message
            {
                MessageHeader = Header,
                MessageBody = Body,
                MessageFooter = Footer
            };
        }
    }
}
