﻿using FunkyErrorHandler.Builder.Model;

namespace FunkyErrorHandler.Builder.Interface
{
    public interface IBuilder
    {
        Message Build();
    }

    public interface IBuilderHeader
    {
        IBuilderBody BuildHeader();
    }

    public interface IBuilderBody
    {
        IBuilderFooter BuildBody();
    }

    public interface IBuilderFooter
    {
        IBuilder BuildFooter();
    }
}
