using FunkyErrorHandler.Builder.Interface;
using FunkyErrorHandler.Builder.Model;
using FunkyErrorHandler.Extensions;
using FunkyErrorHandler.Strategy;
using FunkyErrorHandler.Strategy.Interface;
using System;

namespace FunkyErrorHandler
{
    public class FunctionalErrorHandler
    {
        private static int ERRORCOUNTER = 0;
        private readonly IBuilderHeader _messageBuilder;
        private readonly IMessenger _messenger;


        public FunctionalErrorHandler(IMessenger messenger, IBuilderHeader messageBuilder)
        {
            _messenger = messenger ?? throw new ArgumentNullException(nameof(messenger));
            _messageBuilder = messageBuilder ?? throw new ArgumentNullException(nameof(messageBuilder));
        }

        public Message CreateMessage() =>
            _messageBuilder
            .BuildHeader()
            .BuildBody()
            .BuildFooter()
            .Build();

        public bool SendMessage(Message msg, string to, DeliveryType delivery) =>
            _messenger.Deliver(msg, to, delivery);

        public string SendMessage(string input)
        {
            ERRORCOUNTER++;
            if (ERRORCOUNTER % 2 == 0)
                throw new Exception("Bummer!");
            return $"I made it! Even tho you sent me {input}!";
        }

        public string BuildMessage(Func<string> funcHandle, Func<Exception, string> exceptionHandleAction)
        {
            try
            {

                var builder = new Builder.Builder();
                var message = builder.BuildHeader()
                    .BuildBody()
                    .BuildFooter()
                    .Build();
                return message.ToString();
            }
            catch (Exception ex)
            {
                return exceptionHandleAction(ex);
            }
        }
        

        public Func<Func<int, int, bool>, Action<Exception>, bool> CompareInts = (compare, log) =>
        {
            try
            {
                return compare.Invoke(5, 5);
            }
            catch (Exception ex)
            {
                log(ex);
                return false;
            }
        };



        public Func<Func<string, string>, int, Action<Exception>, string> GetStringAsyncWithRetry = (getFunc, retries, logAction) =>
        {
            for (int i = 0; i <= retries; i++)
            {
                try
                {
                    return getFunc.Invoke("www.google.com");
                }
                catch (Exception ex)
                {
                    logAction(ex);
                }
            }

            return string.Empty;
        };
    }
}
