﻿using System;
using System.Reflection;

namespace FunkyErrorHandler.Extensions
{
    public static class Exceptions
    {
        public static Exception PreserveStackTrace(this Exception exception)
        {
            MethodInfo preserveStackTrace = typeof(Exception).GetMethod("InternalPreserveStackTrace",
            BindingFlags.Instance | BindingFlags.NonPublic);
            preserveStackTrace.Invoke(exception, null);
            return exception;
        }
    }
}

