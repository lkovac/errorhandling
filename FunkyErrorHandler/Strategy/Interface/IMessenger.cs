﻿using FunkyErrorHandler.Builder.Model;

namespace FunkyErrorHandler.Strategy.Interface
{
    public interface IMessenger
    {
        void MessageTypeDelivery(DeliveryType type);

        bool Deliver(Message message, string To, DeliveryType delivery);
    }
}
