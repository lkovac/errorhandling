﻿namespace FunkyErrorHandler.Strategy
{
    public enum DeliveryType
    {
        REGULAR,
        FAST,
        SUPER_FAST,
        FASTER_THAN_MILLENIUM_FALCON
    }
}
