﻿using FunkyErrorHandler.Builder.Model;
using FunkyErrorHandler.Strategy.Interface;
using System;
using System.Collections.Generic;
using System.Threading;

namespace FunkyErrorHandler.Strategy
{
    public class Messenger : IMessenger
    {
        private readonly IDictionary<DeliveryType, Func<Message, bool>> _deliveryStrategy;

        private readonly Func<Message, bool> RegularDelivery = (x) =>
        {
            Thread.Sleep(1000);
            Console.WriteLine($"Regular. Deliverable: {x.ToString()}");
            return true;
        };

        private readonly Func<Message, bool> FastDelivery = (x) =>
        {
            Thread.Sleep(700);
            Console.WriteLine($"Fast. Deliverable: {x.ToString()}");
            return true;
        };

        private readonly Func<Message, bool> SuperFastDelivery = (x) =>
        {
            Thread.Sleep(300);
            Console.WriteLine($"Super Fast. Deliverable: {x.ToString()}");
            return true;
        };

        private readonly Func<Message, bool> MilleniumFalconFastDelivery = (x) =>
        {
            Console.WriteLine($"Millenium Falcon Fast. Deliverable: {x.ToString()}");
            return true;
        };

        private DeliveryType _currentDeliveryType;


        public Messenger()
        {
            _deliveryStrategy = new Dictionary<DeliveryType, Func<Message, bool>>
            {
                { DeliveryType.REGULAR, RegularDelivery },
                { DeliveryType.FAST, FastDelivery },
                { DeliveryType.SUPER_FAST, SuperFastDelivery },
                { DeliveryType.FASTER_THAN_MILLENIUM_FALCON, MilleniumFalconFastDelivery}
            };
        }

        public bool Deliver(Message message, string To, DeliveryType delivery) => 
            _deliveryStrategy[delivery].Invoke(message);

        public void MessageTypeDelivery(DeliveryType type) =>
            _currentDeliveryType = type;

    }
}
