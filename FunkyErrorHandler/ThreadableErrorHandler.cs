﻿using FunkyErrorHandler.Extensions;
using System;
using System.IO;
using System.Threading.Tasks;

namespace FunkyErrorHandler
{
    public class ThreadableErrorHandler
    {
        public async Task<int> AwaitMultipleFailures()
        {
            try
            {
                await CauseMultipleFailures().WithAllExceptions();
            }
            catch (Exception e)
            {
                Console.WriteLine("Caught arbitrary exception: {0}", e);
            }
            // Nothing went wrong, remarkably! 
            return 0;
        }

        private Task<int> CauseMultipleFailures()
        {
            // Simplest way of inducing multiple exceptions 
            Exception[] exceptions = { new IOException(), new ArgumentException() };
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            tcs.SetException(exceptions);
            return tcs.Task;
        }
    }

}
